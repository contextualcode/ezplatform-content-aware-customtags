# eZ Platform Content-Aware Custom Tags

This bundle provides the ability to use different sets of custom tags in Online Editor based on the content location.

It might be useful for huge projects with multiple siteaccesses, where different sets of custom tags are used on different siteacceses. 

## Installation

1. Require via composer:

    ```bash
    composer require contextualcode/ezplatform-content-aware-customtags
    ```
   
2. Clear browser caches and enjoy!

## Usage

After the bundle is installed, it is possible to configure which custom tags should be enabled for different locations. It is done by using `content_aware_custom_tags.settings` configuration.

Let's say we want to:
- Disable `ezyoutube` and `ezfacebook` custom tags for all the content placed under location `#54`
- Enable only `eztwitter` custom tag for all the content placed under location `#55`

In this case we would need to create `config/packages/content_aware_custom_tags.yaml` configuration file:

```yaml
content_aware_custom_tags:
    settings:
        - {parentLocationId: 54, disabled: [ezyoutube, ezfacebook]}
        - {parentLocationId: 55, enabled: [eztwitter]}
```