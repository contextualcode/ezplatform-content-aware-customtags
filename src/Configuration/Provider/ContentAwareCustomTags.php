<?php

declare(strict_types=1);

namespace ContextualCode\EzPlatformContentAwareCustomTags\Configuration\Provider;

use eZ\Publish\Core\MVC\ConfigResolverInterface;
use eZ\Publish\Core\Repository\Values\Content\Location;
use EzSystems\EzPlatformRichText\SPI\Configuration\Provider;
use Symfony\Component\HttpFoundation\RequestStack;

final class ContentAwareCustomTags implements Provider
{
    /** @var array */
    private $settings;

    /** @var ConfigResolverInterface */
    private $configResolver;

    /** @var RequestStack */
    private $requestStack;

    public function __construct(
        array $settings,
        ConfigResolverInterface $configResolver,
        RequestStack $requestStack
    ) {
        $this->settings = $settings;
        $this->configResolver = $configResolver;
        $this->requestStack = $requestStack;
    }

    public function getName(): string
    {
        return 'contentAwareCustomTags';
    }

    public function getConfiguration(): array
    {
        $location = $this->getLocation();
        if (null === $location) {
            return [];
        }

        $disabled = [];
        $allCustomTags = $this->getSiteAccessConfigArray('fieldtypes.ezrichtext.custom_tags');
        $contentSettings = $this->getCustomTagSettingsForContent($location);

        // Disable all the custom tags except the enabled ones
        if (count($contentSettings['enabled'])) {
            foreach ($allCustomTags as $customTag) {
                if (!in_array($customTag, $contentSettings['enabled'], true)) {
                    $disabled[] = $customTag;
                }
            }
        }

        // Disable disabled custom tags
        foreach ($contentSettings['disabled'] as $customTag) {
            if (in_array($customTag, $allCustomTags, true)) {
                $disabled[] = $customTag;
            }
        }

        return ['disabled' => array_values($disabled)];
    }

    private function getLocation(): ?Location
    {
        $request = $this->requestStack->getCurrentRequest();
        if (null === $request) {
            return null;
        }

        $route = $request->attributes->get('_route');

        if ($route === 'ezplatform.content.draft.edit') {
            $location = $request->attributes->get('location');
            if (null === $location && $request->attributes->has('view')) {
                $view = $request->attributes->get('view');
                if ($view->hasParameter('parent_location')) {
                    $location = $view->getParameter('parent_location');
                }
            }

            return $location;
        }

        if ($route === 'ezplatform.content.create_no_draft') {
            return $request->attributes->get('parent_location');
        }

        return null;
    }

    private function getCustomTagSettingsForContent(Location $location): array
    {
        $settings = [
            'enabled' => [],
            'disabled' => [],
        ];

        $path = array_map('intval', $location->path);
        foreach ($this->settings as $locationSettings) {
            if (in_array($locationSettings['parentLocationId'], $path, true)) {
                // Enabled custom tags
                if (count($locationSettings['enabled'])) {
                    foreach ($locationSettings['enabled'] as $customTag) {
                        if (!in_array($customTag, $settings['enabled'], true)) {
                            $settings['enabled'][] = $customTag;
                        }
                    }
                }
                // Disabled custom tags
                if (count($locationSettings['disabled'])) {
                    foreach ($locationSettings['disabled'] as $customTag) {
                        if (!in_array($customTag, $settings['disabled'], true)) {
                            $settings['disabled'][] = $customTag;
                        }
                    }
                }
            }
        }

        return $settings;
    }

    private function getSiteAccessConfigArray(string $paramName): array
    {
        return $this->configResolver->hasParameter($paramName)
            ? $this->configResolver->getParameter($paramName)
            : [];
    }
}
